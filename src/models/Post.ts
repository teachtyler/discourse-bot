export type Post = {
    title: string;
    raw: string;
    category: number;
    tags: string[];
};
