import { Discourse } from './discourse'
import { CategoryChannel } from 'discord.js';
import { Post } from '../../models/Post';
import { RequestPromise } from '../../models/RequestPromise';
import { PostStatus } from '../../models/PostStatus';


export class Topic {
    discourse: Discourse = new Discourse;
    constructor() { }

    createPost = async (
        { title, raw, category, tags }: Post
    ): RequestPromise<Post> =>
        await this.discourse.post<Post>('posts', { title, raw, category, 'tags[]': tags })

    updatePostStatus = async (
        id: any, parameters: any
    ): RequestPromise<PostStatus> =>
        await this.discourse.put<PostStatus>(`t/${id}/status`, parameters)

}