import * as request from 'request';
import * as querystring from 'querystring';

import { Topic } from './topic'
import { RequestPromise, HandleResponse } from '../../models/RequestPromise';
import { Tags } from './tags';

export class Discourse {
    apiKey = process.env.DISCOURSE_KEY
    url = "https://weknow.services"
    username = "discobot"

    constructor() { }

    get<T>(url: string): RequestPromise<T> {
        return new Promise((resolve) => {
            const uri = `${this.url}/${url}?api_key=${this.apiKey}&api_username=${this.username}`

            request.get(uri, (error, response: any, rawBody: string) => {
                const result = this.handleResponse<T>(error, response, rawBody)
                console.log('get', result)
                resolve(result)
            });
        })
    };


    post<T>(url: string, parameters: {}): RequestPromise<T> {
        return new Promise((resolve) => {
            const form = querystring.stringify(parameters)
            const uri = `${this.url}/${url}?api_key=${this.apiKey}&api_username=${this.username}`

            const options = {
                method: 'POST',
                uri,
                form
            }

            request.post(options, (error, response: any, rawBody: string) => {
                const result = this.handleResponse<T>(error, response, rawBody)
                resolve(result)
            });
        })
    };


    put<T>(url: string, parameters: {}): RequestPromise<T> {
        return new Promise((resolve) => {

            const params = querystring.stringify(parameters)
            const postUrl = `${this.url}/${url}?api_key=${this.apiKey}&api_username=${this.username}&${params}`

            request.put(postUrl, (error, response: any, rawBody: string) => {
                const result = this.handleResponse<T>(error, response, rawBody)
                resolve(result)
            });
        })
    }

    handleResponse<T>(error, response, rawBody): HandleResponse<T> {
        if (error) {
            return { error: new Error(error).message, body: undefined };
        } else {
            try {
                const body = JSON.parse(rawBody)
                if (!error && response.statusCode !== 200) {
                    const errors = body.hasOwnProperty('errors') ? body.errors[ 0 ] : ''
                    return { error: new Error(response.statusCode + ' ' + errors).message, body: undefined };
                }
                return { body }
            } catch (e) {
                return { error: new Error(e).message, body: undefined }
            }
        }
    }

    get topic() { return new Topic() }
    get tags() { return new Tags() }
}
