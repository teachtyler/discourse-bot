'use strict';
global.XMLHttpRequest = require('xhr2');
global.WebSocket = require('ws');
// import * as express from 'express';
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const result = require('dotenv').config()
if (result.error) {
    throw new Error(result.error)
}

require("./router")(app);

server.listen(process.env.PORT || 1337);